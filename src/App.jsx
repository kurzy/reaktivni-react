import { Counter, SentenceChanger, Todo, WordCounter } from './components';
import { AppContextProvider } from './context/Context';

const Divider = () => (
  <hr />
);

const AppContent = () => (
  <>
    <h1>Hello world! I am first JSX App!</h1>

    <Divider />

    <div className="row">
      {`${PRODUCTION ? 'Jde' : 'Nejde'} o produkční verzi`}
    </div>

    <Counter />

    <WordCounter />

    <SentenceChanger />

    <Todo />
  </>
);

const App = () => (
  <AppContextProvider>
    <AppContent />
  </AppContextProvider>
);

export default App;
