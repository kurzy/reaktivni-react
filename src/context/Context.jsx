import { createContext, useContext, useEffect, useState } from 'react';

const useAppContext = () => {
  const [count, setCount] = useState(0);
  const [tens, setTens] = useState(0);
  const [words, setWords] = useState([]);
  const [deleteTodo, setDeleteTodo] = useState(() => () => { });

  const store = {
    count,
    deleteTodo,
    tens,
    words,
  };

  const context = {
    store,
    setDeleteTodo,
    setCount,
    setTens,
    setWords,
  };

  return context;
}

const defaultValues = {
  store: {
    count: 0,
    deleteTodo: () => { },
    tens: 0,
    words: [],
  },
  setDeleteTodo: () => { },
  setCount: () => { },
  setTens: () => { },
  setWords: () => { },
};

const AppContext = createContext(defaultValues);
AppContext.displayName = 'AppContext';

export const AppContextProvider = ({
  children,
}) => {
  const state = useAppContext();

  const { Provider } = AppContext;

  return (
    <Provider value={state}>
      {children}
    </Provider>
  );
};

export const useAppContextState = () => useContext(AppContext);
