import { memo, useEffect, useMemo, useState } from 'react';
import Button from '../../Button';
import { useAppContextState } from '../../../context/Context';

const computeWordLength = (someWord) => {
  /* let i = 0;
  while (i < 5E9) {
    i++;
  }
  */
  if (typeof someWord === 'undefined') {
    return 0;
  }

  return [...someWord].length;
};

const WordCounterContent = () => {
  const [count, setCount] = useState(0);
  const [wordIndex, setWordIndex] = useState(0);
  const {
    store: { words },
  } = useAppContextState();

  useEffect(
    () => setWordIndex(0),
    [words],
  );

  const word = useMemo(
    () => words[wordIndex] ?? '-',
    [wordIndex, words],
  );

  const wordLength = useMemo(
    () => computeWordLength(word),
    [word],
  );

  return (
    <>
      <h2>Počet slov</h2>
      <p>{words.join(', ')}</p>

      <p>
        {`Slovo ${word} má ${wordLength} písmen.`}
      </p>

      <Button
        onClick={() => {
          const newIndex = wordIndex + 1;

          const resolvedIndex = newIndex >= words.length
            ? 0
            : newIndex;

          setWordIndex(resolvedIndex);
        }}
      >
        Další slovo
      </Button>

      <h2>Rendering</h2>

      <p>{`Počitadlo: ${count}`}</p>

      <Button
        onClick={() => setCount(count + 1)}
      >
        Překresli
      </Button>
    </>
  );
};

export default memo(WordCounterContent);
