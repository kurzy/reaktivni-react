import { memo } from 'react';
import { WordCounterContent } from './components';

const WordCounter = () => (
  <div className="row">
    <WordCounterContent />
  </div>
);

export default memo(WordCounter);
