import { memo, useEffect, useState } from 'react';
import Button from './Button';
import { useAppContextState } from '../context/Context';

const sentences = [
  'Tohle👍💕 je fakt dost hustý',
  'Za všechno může Kalousek',
  'Plníme sliby na 93 procent',
  'Praha je velmi hezké město',
  'Brno je zlatá loď',
  'Odpověď na základní otázku vesmíru života a vůbec je 42',
];

const SentenceChangerContent = () => {
  const [index, setIndex] = useState(0);
  const { setWords } = useAppContextState();

  useEffect(
    () => setWords(sentences[0].split(' ')),
    [],
  );

  const onClickHandler = () => {
    const newIndex = index + 1;
    const resolvedIndex = newIndex >= sentences.length
      ? 0
      : newIndex;
    
    setIndex(resolvedIndex);

    setWords(sentences[resolvedIndex].split(' '));
  }

  return (
    <>
      <h2>Změna věty</h2>

      <p>{`Aktuální věta: ${sentences[index]}`}</p>

      <Button variant="secondary" onClick={onClickHandler}>
        Změň větu
      </Button>
    </>
  );
};

const SentenceChanger = () => (
  <div className="row">
    <SentenceChangerContent />
  </div>
);

export default memo(SentenceChanger);
