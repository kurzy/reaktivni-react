const CountView = ({
  count = 0,
}) => (
  <div className="view">
    {`Počet kliknutí ${count}`}
  </div>
);

export default CountView;
