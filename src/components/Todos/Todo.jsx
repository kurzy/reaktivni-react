import TodoList from './TodoList';
import Button from '../Button';
import { useEffect, useState } from 'react';
import { addTodo, deleteTodo, getTodos } from './utils/todo';
import { useAppContextState } from '../../context/Context';

const Todo = () => {
  const [items, setItems] = useState(getTodos());
  const { setDeleteTodo } = useAppContextState();

  
  useEffect(
    () => {
      setDeleteTodo(() => (index) => {
        deleteTodo(index);
        setItems(getTodos());
      });
    },
    [setDeleteTodo],
  );

  return (
    <>
      <div className="row">
        <TodoList items={items} />
      </div>
      <div className="row">
        <Button
          variant="secondary"
          onClick={() => {
            addTodo();
            setItems(getTodos());
          }}
        >
          Přidat úkol
        </Button>
      </div>
    </>
  );
};

export default Todo;
