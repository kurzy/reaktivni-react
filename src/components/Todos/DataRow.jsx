import clsx from 'clsx';

const DataRow = ({
  text = '', strike = false, updated = false, onChange,
}) => (
  <div className={clsx('data-row', strike && 'strike')}>
    {updated
      ? (
        <input
          type="text"
          value={text}
          onChange={(event) => {
            onChange(event.target.value);
          }}
        />
      )
      : text}
  </div>
);

export default DataRow;
