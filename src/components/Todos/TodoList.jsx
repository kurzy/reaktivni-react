import { useEffect, useState } from 'react';
import TodoItem from './TodoItem';

const TodoList = ({ items = [] }) => {
  const [todos, setTodos] = useState([]);

  useEffect(
    () => {
      if (Array.isArray(items)) {
        setTodos(items);
      }
    },
    [items],
  )

  return (
    <div className="todo-list">
      {todos.map((item) => <TodoItem todoItem={item} key={item.index} />)}
    </div>
  );
};

export default TodoList;
