const CheckBox = ({ checked = false, onChange }) => (
  <input type="checkbox" checked={checked} onChange={onChange} />
);

export default CheckBox;
