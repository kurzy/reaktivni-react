import CheckBox from './CheckBox';
import DataRow from './DataRow';
import { useEffect, useState } from 'react';
import { useAppContextState } from '../../context/Context';
import { updateTodo } from './utils/todo';
import Button from '../Button';

const TodoItem = ({ todoItem }) => {
  const { done, index, text } = todoItem;

  const { store: { deleteTodo } } = useAppContextState();

  const [checked, setChecked] = useState(done);
  const [itemText, setItemText] = useState(text);
  const [updated, setUpdated] = useState(!text);

  const onChangeCheckBox = () => {
    if (!itemText) {
      return;
    }

    const newChecked = !checked;
    setChecked(newChecked);
    setUpdated(false);
    updateTodo({...todoItem, done: newChecked, text: itemText});
  };

  const onUpdateClick = () => {
    if (!checked) {
      if (updated) {
        updateTodo({...todoItem, done: checked, text: itemText});
      }

      setUpdated(!updated);
    }
  }

  return (
    <div className="todo-item">
      <CheckBox checked={checked && !!itemText} onChange={onChangeCheckBox} />
      
      <DataRow
        text={itemText}
        strike={checked}
        updated={updated}
        onChange={setItemText}
      />
      
      <Button variant="primary" disabled={!itemText || checked} onClick={onUpdateClick}>
        {updated ? 'Hotovo' : 'Upravit'}
      </Button>
      
      <Button onClick={() => {
        deleteTodo(index);
      }}
      >
        Smazat
      </Button>
    </div>

  );
};

export default TodoItem;
