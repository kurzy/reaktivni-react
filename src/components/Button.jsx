import clsx from 'clsx';

const Button = ({
  children,
  className,
  disabled = false,
  large = false,
  onClick = () => { },
  variant,
}) => (
  <button
    className={clsx('react-button', variant, large && 'large', className)}
    disabled={disabled}
    onClick={() => onClick(1)}
    type="button"
  >
    {children}
  </button>
);

export default Button;
