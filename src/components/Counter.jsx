import { useCallback } from 'react';
import { useAppContextState } from '../context/Context';
import Button from './Button';
import CountView from './CountView';

const Counter = () => {
  const {
    store: { count, tens },
    setCount, setTens,
  } = useAppContextState();

  const onClickHandler = useCallback(
    (data) => {
      const newCount = count + data;

      setCount(newCount);

      const newTens = Math.floor(newCount / 10);
      setTens(newTens);
    },
    [count],
  );

  return (
    <div className="row">
      <span>{tens}</span>

      <Button
        className="pill"
        large
        onClick={onClickHandler}
        variant="secondary"
      >
        <CountView count={count} />
      </Button>
    </div>
  );
};

export default Counter;
