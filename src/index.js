import { Fragment, createElement } from 'react';
import './css/index.css';
import { createRoot } from 'react-dom/client';

const component = createElement(
  Fragment,
  undefined,
  createElement('h1', undefined, 'Hello word, I am first React App!'),
  createElement(
    'div',
    { className: 'row' },
    `${PRODUCTION ? 'Jde' : 'Nejde'} o produkční verzi`,
  ),
);

const root = createRoot(document.querySelector('#app'));
root.render(component);
