/** @type { import("eslint").Linter.Config[] } */
import rjConfig from 'eslint-config-regiojet-vanilla';

const eslintConfig = [
  {
    ignores: [
      'dist/',
    ],
  },
  {
    languageOptions: {
      globals: {
        PRODUCTION: 'writable',
      },
    },
  },
  ...rjConfig,
  {
    settings: {
      'import/resolver': {
        webpack: {
          config: './webpack.config.prod.js',
        },
      },
    },
    rules: {
    },
  },
];

export default eslintConfig;
