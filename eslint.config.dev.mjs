/** @type { import("eslint").Linter.Config[] } */
// eslint-disable-next-line import/extensions
import prodConfig from './eslint.config.mjs';

const eslintConfig = [
  ...prodConfig,
  {
    settings: {
      'import/resolver': {
        webpack: {
          config: './webpack.config.dev.js',
        },
      },
    },
    rules: {
      'no-console': [
        'warn',
        {
          allow: ['warn', 'error'],
        },
      ],
    },
  },
];

export default eslintConfig;
